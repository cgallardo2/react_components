import React, {useState} from 'react'
import "./counter.css"

function Counter(){
    const [count,countState] = useState(0);
    return (
      <div className="counter">
        <h1> Counter component</h1>
        <h1
          className={`numberSize ${count>0 ? "positive" : count <0 ? "negative": null}`}>
              
          {count}
        </h1>
        <div className="button__wrapper">
          <button onClick={() => countState(count - 1)}>-</button>
          <button onClick={() => countState(count + 1)}>+</button>
        </div>
      </div>
    );
}

export default Counter;