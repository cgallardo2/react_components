import logo from './logo.svg';
import './App.css';
import Accordion from './components/Accordion';
import Counter from './components/counter';
import Slider from './components/Slider/Slider';
function App() {
  return (
    <div className="App">
      <div class="App-header">
        <img src={logo} height="250px" width="250px" />
        <h1 class="Title">Components made with React </h1>
        <h3> hello world!</h3>
      </div>

      <Counter />
      <Accordion
        title="This is an accordion (click me to see more text)"
        content="Importante text about the accordion "
      />

      <Slider />
    </div>
  );
}

export default App;
